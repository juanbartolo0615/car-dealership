from django.urls import path
from .views import api_salespeople, api_sale, api_customer


urlpatterns = [
    path(
        'salespeople/',
        api_salespeople,
        name='api_salespeople'
    ),
    path(
        'salespeople/<int:pk>/',
        api_salespeople,
        name='api_salesperson'
    ),
    path(
        'sales/',
        api_sale,
        name='api_sales'
    ),
    path(
        'sales/<int:pk>/',
        api_sale,
        name='api_sale'
    ),
    path(
        'customers/',
        api_customer,
        name='api_customers'
    ),
    path(
        'customers/<int:pk>/',
        api_customer,
        name='api_customer'
    ),
]

