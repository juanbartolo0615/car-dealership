from django.contrib import admin
from .models import Customer, Sale, Salesperson, AutomobileVO

# I cant believe our amazing mod 2 instructors didn't teach us to register admin pages this way </3

admin.site.register(Customer)
admin.site.register(Salesperson)
admin.site.register(AutomobileVO)
admin.site.register(Sale)