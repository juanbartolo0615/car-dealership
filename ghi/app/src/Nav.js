import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="navbar-nav me-auto mb-2 mb-lg-0" style={{flexWrap: 'wrap', padding: '10px', alignItems: 'baseline'}}>
            <NavLink className="navbar-brand" to="/" style={{padding: '10px'}}>CarCar</NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/manufacturers">Manufacturer List</NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/manufacturers/new">Create a Manufacturer</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='models'>Models</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='models/new'>Create a Model</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='automobiles'>Automobiles</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='automobiles/new'>Create an Automobile</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='salesperson'>Salespeople</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='salesperson/new'>Add a Salesperson</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='customers'>Customers</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='customers/new'>Add a Customer</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='sales'>Sales</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='sales/new'>Add a Sale</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='sales/history'>Salesperson History</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='technicians/'>Technician List</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='technicians/new'>Technician Form</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='appointments/'>Appointment List</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='appointments/new'>Appointment Form</NavLink>
              <NavLink className="nav-link active" aria-current="page" to='appointments/history'>Service History</NavLink>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
