import React from "react";

function AppointmentList({ appointments, getAppointments }) {
    function vipCheck(vip) {
        if (vip) {
            return "Yes";
        } else {
            return "No";
        }
    }
    async function cancel(id){
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const response = await fetch(url, {method: "PUT"});
        if (response.ok){
            getAppointments();
        } else {
            console.log('Error fetching update')
        }
    }
    async function finish(id){
        const url = `http://localhost:8080/api/appointments/${id}/finish/`;
        const response = await fetch(url, {method: "PUT"});
        if (response.ok){
            getAppointments();
        } else {
            console.log('Error fetching update')
        }
    }


    return (
        <div className="container">
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        if (appointment.status === "created") {
                            return (

                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{vipCheck(appointment.vip)}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button onClick={() => cancel(appointment.id)} type="button" className="btn btn-danger">Cancel</button>
                                        <button onClick={() => finish(appointment.id)} type="button" className="btn btn-success">Finish</button>
                                    </td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </div>
    );

}


export default AppointmentList;
