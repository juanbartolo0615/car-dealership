import React, { useState } from "react";
import { useNavigate } from "react-router-dom";


function ManufacturerForm({ getManufacturers }) {
    const navigate = useNavigate()
    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({name}),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok){
            setName('');
            getManufacturers();
            navigate('/manufacturers')
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="display-6">Create a Manufacturer</h1>
                    <div className="container">
                        <form onSubmit={handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Manufacturer name...</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default ManufacturerForm;
