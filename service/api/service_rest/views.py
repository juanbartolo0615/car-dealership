from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
# Create your views here.
class AutoMobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "vip",
        "customer",
        "date",
        "time",
        "technician",
        "reason",
        "status",
        "id",
    ]
    encoders={
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["DELETE", "GET", "POST"])
def technician(request, technician_id=None):
    if request.method == "GET" and technician_id == None:
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    elif request.method == "GET" and technician_id != None:
        try:
            technician = Technician.objects.get(id=technician_id)
            return JsonResponse(
                technician,
                TechnicianEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Invalid Technician id"},
                status=400,
            )

    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                TechnicianEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Invalid JSON Body"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=technician_id).delete()
        return JsonResponse({"deleted": count > 0})



@require_http_methods(["DELETE", "GET", "POST"])
def appointments(request, appointment_id=None):
    if request.method == "GET" and appointment_id==None:
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "GET" and appointment_id != None:
        try:
            appointment = Appointment.objects.get(id=appointment_id)
            return JsonResponse(
                appointment,
                AppointmentEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Invalid Appointment ID"},
                status=400,
            )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            try:
                technician = Technician.objects.get(id=content["technician"])
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                {"message": "Invalid Technicican ID"},
                status=400,
                )
            auto_true = AutomobileVO.objects.filter(vin=content["vin"])
            if auto_true == True and auto_true["sold"] == True:
                content["vip"] = True
            content["status"] = "created"
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                AppointmentEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Invalid JSON Body"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=appointment_id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["PUT"])
def canceled(request, appointment_id):
    try:
        Appointment.objects.filter(id=appointment_id).update(status="canceled")
        appointment = Appointment.objects.get(id=appointment_id)
        return JsonResponse(
            appointment,
            AppointmentEncoder,
            safe=False
        )
    except:
        return JsonResponse(
            {"message": "Invalid Appointment ID"},
            status=400,
        )


@require_http_methods(["PUT"])
def finished(request, appointment_id):
    try:
        Appointment.objects.filter(id=appointment_id).update(status="finished")
        appointment = Appointment.objects.get(id=appointment_id)
        return JsonResponse(
            appointment,
            AppointmentEncoder,
            safe=False
        )
    except:
        return JsonResponse(
            {"message": "Invalid Appointment ID"},
            status=400,
        )
